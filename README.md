# cells-tutorial-trivial

Your component description.

Example:
```html
<cells-tutorial-trivial></cells-tutorial-trivial>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-tutorial-trivial  | Mixin applied to :host     | {}  |
| --cells-fontDefault  | Mixin applied to :host font-family    | sans-serif  |
