## Utilizando el cells demo helper para conseguir demos de componentes utiles

#### descripción
El componente [cells-demo-helper](https://globaldevtools.bbva.com/bitbucket/projects/CEL/repos/cells-demo-helper/browse) es un componente generado por el equipo de catalogo para hacer demos de forma rapida sencilla y efectiva.

Cuando creas un componente desde cero en la carpeta demo del componente sin que necesites hacer nada se genera una demo por defecto con tu componente ya puesto, cuando ya has implementado la funcionalidad llega el momento de poenr ejemplos de como utilizarlo para que los demas puedan facilmente reaprovechar tu componente.

###### ejemplo de como se ve una demo

![](images/ejemplo_1.png)



#### que tiene una demo 

Puedes ver que una demos tiene las siguientes partes:

- titulo sobre que ejemplo tienes selecionado
- subtitulo del ejemplo que tienes selecionado 
- dropdown para escoger que ejemplo quieres mostrar, ya que puedes tener varios y enseñar los casos de uso de tu compoente  
- dropdown para escoger que idioma quieres utilizar, ya que debes de hacer que tu componente se pueda traducir a los diferentes idiomas (al menos español e ingles)
- dropdown para escoger que theme quieres utilizar, para ver como se veria tu componente con el theme base, con el de glomo, el de ngob..
- un toogle para cambiar entre los modos "con viweport"  que te permite ver tu componente como se veria en movil tables... y sin nada
- una pestaña para ver el codigo con el que se esta construyendo la demo que tienes selecionada 
- la parte de abajo donde ves tu componente come se veria en una aplicacion  

#### que tiene un componente creado desde cero 

por defecto veras que tienes una carpeta css con un fichero demo-styles.css y un index.html con la demo por defecto

El contenido del index importante es:

Un script que sirve para indicar que la demo utilizara shadow-dom para que se vea como se comporta tu componente de forma encapsulada 

Se crea la variable I18nMsg que tiene los literales que pongas en la carpeta de literales, ojo, en el pasado creabamos una carpeta dentro de la demo para los literales de la demo y que fueran diferentes de los de que se usaban en la app, ahora se usan los mismos y lo que se hace es los literales de tu app se ponen en la app y "pisan" los que esten en el componente si se llaman igual 

-
~~~ 
    <script>
      window.Polymer = {
        dom: 'shadow',
        lazyRegister: true,
        useNativeCSSProperties: true
      };
      window.I18nMsg = {}; window.I18nMsg.url = '../locales';
    </script>
~~~

Importamos los polyfils para que si el navegador con el que se ve no soporta webcomponents de forma nativa el componete funciona correctamente (es importante que antes de poner un componente para certificar pruebes que funciona en chrome, firefox edge y safary)

-
~~~ 
    <script src="../../webcomponentsjs/webcomponents-lite.js"></script>
~~~

Despues se importan tu componente,los compoentes que van a usarse en la demo y los estilos de la carpeta css 

-
~~~ 
    <link rel="import" href="../../cells-demo-helper/cells-demo-helper.html">
    <link rel="import" href="../cells-tutorial-demo.html">
    <link rel="import" href="css/demo-styles.html">
~~~

Esto es lo por defecto, tu tendras que añadir los iconos que uses, los themes, otros componentes que pongas en los <slot>  de tu componente etc...

Ejemplo de lo que se importa en [cells-icon-message](https://globaldevtools.bbva.com/bitbucket/projects/BGCM/repos/cells-icon-message/browse/demo/index.html)

-
~~~ 
    <link rel="import" href="../../coronita-icons/coronita-icons.html">
    <link rel="import" href="../../cells-demo-helper/cells-demo-helper.html">
    <link rel="import" href="../../cells-demo-mocker/cells-demo-mocker.html">
    <link rel="import" href="../../paper-dropdown-menu/paper-dropdown-menu-light.html">
    <link rel="import" href="../../cells-atom-button/cells-atom-button.html">
    <link rel="import" href="../cells-icon-message.html">
    <link rel="import" href="css/demo-styles.html">
~~~

> -- Importante  
> Recuerda que debes de importar en el bower.json todas las dependencias que pongas aqui en la seccion devDependencies con --save-dev al hacer bower i, para evitar que cosas que solo usas en la  demo acaben como peso extra de tu componente en producción.



A continuacion viene el body de tu app 

-
~~~ 
<body unresolved ontouchstart>
~~~

 - El atributto ontouchstart sirve para que el hover se muestre cuando haces touch en movil 
 - El atributto unresolved lo cambia polymer cuando esta listo y sirve para saber si la app ya puede empezar



Despues viene el componente 
En el pondras atributos para indicar que opciones (combos) quieres usar en tu demo ya que no todos son obligatorias aunque si recomendables

-
~~~ 
<cells-demo-helper i18n>
     <cells-demo-case heading="example1">...</cells-demo-case>
     <cells-demo-case heading="example2">...</cells-demo-case>
     <cells-demo-case heading="example3">...</cells-demo-case>
     <cells-demo-case heading="example4">...</cells-demo-case>
</cells-demo-helper>
~~~


Ejemplo de demo con varias opciones 

-
~~~ 
 <cells-demo-helper i18n remember-resizable-bars-preference available-themes='["cells-coronita-theme", "cells-banking-theme"]'>
</cells-demo-helper>
~~~

Las opciones que puedes poner aqui estan detalladas en la documentacion del [cells-demo-helper](https://globaldevtools.bbva.com/bitbucket/projects/CEL/repos/cells-demo-helper/browse) 

#### indicar la resolución:

Setar una resolucion por defecto 

-
~~~ 
<cells-demo-helper resolution="tablet">
~~~


Apagar la opcion de ver el componente dentro del viewport (util en fase de desarrollo)

-
~~~ 
<cells-demo-helper resizable-viewport-off>
~~~


personalizar los breackpoints de la demo y añadir los tuyos propios 

-
~~~ 
<cells-demo-helper resolution="Mobile" breakpoints='{
    "mobile": { "width": 360, "height": 640, "name": "Mobile" },
    "tablet": { "width": 768, "height": 1024, "name": "Tablet" }}'>
~~~

Hacer que se guarde en localstorage la opcion que has clickeado en el toogle de forma que la proxima vez aparezca o no dependiendo de lo que hiciste la ultima vez

-
~~~ 
<cells-demo-helper remember-resizable-bars-preference></cells-demo-helper>
~~~


#### indicar el idioma:

Poniendo el atributo i18n en el tag le indicas al componente de demo que quieres que se muestre es combo de selecion de idioma (recuerda que necesitas haber importado el behaviour en tu componete y usar la funcion de traducción para que funcione, asi como tener los jsons de los diferentes idiomas que quieras que se vena en la demo)

-
~~~ 
<cells-demo-helper i18n>
~~~

Indicar que idiomas quieres que aparezcan en el combo, esto es muy util si tienes idiomas locales como en-US o es-MX, pero necesitas que los jsons existan y esten creados con las cadenas de traduccion que usas, si solo ves la cadena de traduccion es que te falta poner el fichero o la cadena para ese idioma, consulta con tu arquitecto que idomas debes de soportar para tu app  

-
~~~ 
<cells-demo-helper i18n langs='["en","es"]'>
~~~


Cambiar el path donde tienes los literales, para por ejemplo tener en tu demo tu propio directorio de literales para la demo y que no viajen y se lleven como peso muerto en todas las aplicaciones que usen tu componente, o para migrar rapido un componente que ya tenias los literales en otra carpeta

-
~~~ 
<cells-demo-helper i18n locales-path="../locales">
~~~

#### eventos

Los componentes pueden disparar eventos para indicar a las aplicaciones que el usuario ha realizado una accion, por ejemplo un click o in key press, generalmente los eventos seran recogidos por el json de configuracion y pasados como in a otros componentes.

Como la demo debe de ser independiente de donde se utilice una buena forma de mostrar que eventos dispara tu componente es mostrar un toast o notificacion donde se vea que el evento ha sido disparado y con que eventos.

El componente de demo te permite indicar que eventos dispara tu componentes y pone un lister por ti y se encarga de enseñar la notificacion de forma visual.

En el ejemplo de la documentacion puedes ver que se hace que se muestre un toast cuando se lancen los eventos de abrir y de cerrar

-
~~~ 
<cells-demo-helper events='["my-component-event-open","my-component-event-close"]'>
~~~

Dependiendo de los casos de uso tu componente puede dispara unos eventos u otros por lo que registra aqui los eventos de todos los eventos y asi funcionara para todos tus "cells-demo-case"

#### temas 

Los themas son estilos propios de las diferentes aplicaciones, es donde modificas el look and feel de todos los componetes que utilizas para que tengan un aspecto homogeneo.

Puedes por ejemplo configurar que todos los links son en azul claro y sin subrayado o que la sombra de todas las cards tiene un color concreto.

Cuando diseñas un componente lo debes de hacer poniendo estilos para que quede bien sin ningun theme, generalmente hacemos trampa y ese aspecto por defecto es el que nos piden para nuestra aplicacion.

Pero debes de dejar tu componente abierto para que sea sencillo ser extendido desde el theme, para ello utilizaras @var  y @apply de forma que se pueda cambiar cosas comunes como los colores y tamaños y mixins para cambiar varias cosas a la vez.

en la demo puedes enseñar como se veria tu componente con diferentes themes como el por defecto, el de glomo o el de ngob.

cuando se utiliza un nuevo componente por primera vez en una aplicacion es bastante probable que el theme no tenga ningun efecto en el aspecto del componete ya que no se establecen por defecto en el theme las variables y mixins especificadas en tu componente.

Aunque es una buena idea que los diferentes themes tengan definidas las mismas variables base como colores primarios, tamaños de fuentes, animaciones y efectos.

en el ejemplo de la documentacion se enseña como añadir dos themes a l combo de themes de tu demo y como especificar cual mostrar por defecto

-
~~~ 
<cells-demo-helper available-themes='["cells-coronita-theme","cells-banking-theme"]' default-theme="cells-banking-theme">
~~~

> -- Importante 
> Recuerda que debes de importar en el bower.json los themes que quieras que aparezcan en el combo en la seccion devDependencies, asi como poner el import en el index.html de la demo, si no no funcionara ni veras los cambios


#### ocultando el ui de la demo 

Mientras desarrollas se puede hacer pesado tener ocupado todo el espacio del interfaz del helper de la demo, hay una clase que te permite ocultarlo temporalmente de forma comoda 

-
~~~ 
<cells-demo-helper class="no-ui"></cells-demo-helper> 
~~~

## generando casos de uso 

Una de las cosas mas importantes de las demos es que se vea el componete en sus diferentes psoibles usos, para ello el componente de cells-demo-helper te deja poner dentro del <slot> diferentes cells-demo-case de forma que puedas ver cada uno de los casos de forma facil.

dependiendo del caso que elijas en el combo se eliminan los demas del html de la demo  tambien es impotante decir que el combo de los casos lo genera el componente de demo automaticamente a partir del content que pongas

la estuctura basica es:

-
~~~ 
      <cells-demo-case heading="Basic" description="A basic example">
        <template>
... tu contenido aqui
        </template>
      </cells-demo-case>
~~~

con los parametros heading y description se indica como es el caso que estas poniendo, se cambia en el titulo y el subtitulo de la zona arriba a la izquierda cuando cambias el combo y se usa para rellenar como se ve el caso en el combo.

trata de ser claro con el heading y da suficiente informacion en la descripción

#### Que poner dentro del template

El ejemplo que trae por defecto intenta ser explicativo, dentro de la tempalte pondras tu componente y otro contenido que pueda ser util para que se vea tu componente, como estilos o datos de mock

Generalmente buscas un ejemplo mas completo de como se muestran los  casos como href="https://globaldevtools.bbva.com/bitbucket/projects/BGCM/repos/cells-icon-message/browse/demo/index.html" target="_blank">cells-icon-message</a> y remplazas el contenido que ponen por defecto para poner el tuyo.

> -- Importante 
> Recuerda que debes de escapar los caracteres especiales que quieras que aparezcan en la descripcion, por ejemplo 
> Use & quot;src& quot; attribute instead of & quot;icon& quot; to display an image.



por defecto empezarias poniendo tu componente con los valores que esperas "a mano"

-
~~~ 
          <cells-icon-message
            src="http://icons.iconarchive.com/icons/dakirby309/simply-styled/128/Google-Drive-icon.png"
            icon-label="Google Drive"
            icon-size="50"
            message="Lorem ipsum dolor sit amet">
          </cells-icon-message>
~~~

Y verificas que se ve correctamente.

Hay un componente que es bastante comodo de usar cuando queires en una demo mostrar diferentes casos de uso con el mismo contenido, pones el json como valor del atributo data

-
~~~ 
            <cells-demo-mocker key="[[key]]" selected="{{selected}}" data='{
              "info": {
                "type": "info",
                "icon": "coronita:info",
                "message": "Lorem ipsum dolor sit ametuidem voluptatum asperiores autem pariatur."
              },
              "warning": {
                "type": "warning",
                "icon": "coronita:alert",
                "message": "Lorem ipsum dolor sit amet."
              },
              "error": {
                "type": "error",
                "icon": "coronita:error",
                "message": "Lorem ipsum dolor sit amet, consectetur."
              },
              "success": {
                "type": "success",
                "icon": "coronita:correct",
                "message": "Lorem ipsum dolor sit amet."
              }
            }'></cells-demo-mocker>
~~~

y puedes usar un combo para cambiar los diferentes valores 

-
~~~
            <paper-dropdown-menu-light label="Type" noink no-animations dynamic-align>
              <paper-listbox class="dropdown-content" selected="{{key}}" attr-for-selected="data-mock" fallback-selection="info">
                <paper-item data-mock="info">Info</paper-item>
                <paper-item data-mock="warning">Warning</paper-item>
                <paper-item data-mock="error">Error</paper-item>
                <paper-item data-mock="success">Success</paper-item>
              </paper-listbox>
            </paper-dropdown-menu-light>
~~~

Cambiando los valores "a mano" por la variables que cambian 

-
~~~
            <cells-icon-message class="inline"
              type="[[selected.type]]"
              icon="[[selected.icon]]"
              icon-size="18"
              message="[[selected.message]]">
            </cells-icon-message>
~~~

> -- Importante 
> Recuerda que debes de utilizar <template is="dom-bind">...</template> para que entienda que debe de bindear las variables automaticamente entre componentes

#### cambiar los valores con un script
Otra forma mas "manual" de pasar valores a tu componente es utilizar una etiqueta de script debajo del componente que has puesto, suele ser practico poner una funcion autoejecutable para que el codigo se auto lance y no necesites poner un boton o combo que cambie los valores.

-
~~~
          <script>
            (function(demo) {
              demo.description = {'value': '98761234', 'masked': true};
              demo.primaryAmount = {'label': 'Disponible', 'amount': 3100, 'currency': 'EUR'};
              demo.secondaryAmount = {'label': 'Dispuesto', 'amount': 45.00, 'currency': 'EUR'};
            }(document.getElementById('demo')));
          </script>
~~~

#### poner los scripts en un js separado

En demos anteriores se generaba un fichero separado de javascript con el codigo para inicializar los componentes, cuando tienes muchas demos, el codigo del html del demo se hace muy grande, pero es necesario pensar que todo lo que ponemos dentro del cells-demo-case se muestra en la pestaña de code y que los usuarios pueden selecionar y copiar ese codigo de ejemplo.

Puedes generarte un fichero separado, pero recuerda indicar de forma sencilal como son los datos que se pasan al objeto en al menos el primero de los casos para que sea mas facil para otros usuarios.

Ademas por la forma en la que el componente de demo "pone" el caso en el iframe tendras que considerar cosas como que el caso este visible y que los componetes esten cargados, no he encontrado un ejemplo que lo haga separando el js en otro fichero.

#### poner los mocks en un fichero separado

Una practica que se hacia tambien  anteriormente era generar un fichero moks.js enel directorio de las demos para tener separados los datos, puedes ver un ejemplo en 
[cells-percentage-bar-balance](https://globaldevtools.bbva.com/bitbucket/projects/BGCM/repos/cells-percentage-bar-balance/browse/demo) 



de forma que tenias dentro de la variable mocks.nombreDeComponete.casoxxx los datos que neceistabas

-
~~~
var mock = mock || {};
mock.percentajeBar = {};
mock.percentajeBar.okResponse = {
  'info': {
    'init': [
      ...
    ],
    'end': [
    ...
    ]
  },
  'progresBar': {
   ...
  },
  'leyend': {
    ...
  }
};

mock.percentajeBar.advancedResponse = {
  'info': {
    'init': [
      {
      ....
      }
    ]
  }
}
~~~

en el html de la demo te traias el script 

-
~~~
    <script src="./mocks/mocks.js"></script>
~~~

y podias utilizar 

-
~~~
  myEl.set('info', mock.percentajeBar.okResponse.info);
~~~

#### simular un theme 

Para emular el efecto de un theme y que los desarrolladores de otras aplicaciones puedan saber que poner para cambiar la configuracion puedes poner un custom style dentro del template y enseñar como poner las clases al componente 

-
~~~
 <style is="custom-style">
     
            .inline-reversed {
              flex-direction: row-reverse;
              text-align: right;

              --cells-icon-message-icon: {
                margin-left: 10px;
                margin-right: 0;
                border-radius: 50%;
                overflow: hidden;
                border: 2px solid #004481;
                background-color: #004481;
              };
            }
</style> 
~~~

y debajo tu componente con la clase 

-
~~~
          <h2 class="demo-heading">Reversed direction via styles</h2>
          <cells-icon-message
            class="inline inline-reversed"
            src="https://randomuser.me/api/portraits/men/84.jpg"
            icon-size="45">
            <div>
              <div>FirstName LastName</div>
              <a href="mailto:user@email.com">user@email.com</a>
            </div>
          </cells-icon-message>
~~~


## que poner en los estilos

Por defecto los estilos que se generan en la demo son muy basicos, en versiones anteriores del cells-cli se ponia bastante contenido y necesitabas borrarlos o sobrescribirlos

Estilos que pone en la version 0.7.5 son:

-
~~~
<style is="custom-style">
  body {
    margin: 0;
  }

  @media (max-width: 768px) {
    *, *:before, *:after {
      -webkit-tap-highlight-color: transparent;
    }
  }
</style>
~~~

Yo personalmente no pongo estilos aqui que afecten a la demo, los otros desarrolladores despues se vuelven locos buscando que esta dando ese pading o color o porque copiando el mismo codigo en su pagina no se ve correctamente.





## tips and tricks

 - Cuando quieres ver varias copias e tu componente a la vez a veces es util poner una regilla responsive para poder ver como se comporta de forma mas similar a como seria en una aplicacion real el componete es [cells-grid-template](https://globaldevtools.bbva.com/bitbucket/projects/CT/repos/cells-grid-template/browse)
 - Genera siempre un ejemplo de tu componente en el que cambies bastante el aspecto y el layout del mismo para que otros developers puedan ver de forma practica como pisar los estilos de tu componete.
 - Genera diferentes ejemplos de tu componente en los que no te pasen todas las propiedades para mostrar como se comporta tu componente en ausencia de datos.
 - Recuerda añadir clases a elementos clave de tu componete y sus correspondientes @apply para que aunque tu no necesites cambiarles los estilos otros puedan hacerlo comodamente.
 - Muestra como se ve tu componete en diferentes resoluciones, si en tamaño escritorio el 80% de tu componente esta en blanco el componente no esta pensado para ser responsive, muestra en su lugar como poner varios en fila.
 - Piensa que aunque ux te de unos tamaños fijos el espacio disponible para el componente en otras aplicaciones no tiene que ser "300px" de ancho, por lo que intenta en las demos mostrar los escenarios posibles, usando cosas como min-width en vez de un valor fijo y recuerda dejar @apply para que otros puedan cambiarlo para sus casos.
 - Pon eventos a las interaciones aunque tu no las uses, por ejemplo un link puede ser que sea una navegacion en la propia pagina, una navegacion a target blank o solo diparar un evento y no navegar en absoluto.
 - Deja tambien que te puedan pasar los nombres de los eventos y sobrescribir los valores por defecto que dejas (especialemente util para casos en los que se tenga mas de una instancia del componente a la vez)
 - Si pones iconos en tu componente aveces compensa dejar que el icono sea opcional o que en vez de un icono se pase una imagen
 - Si tienes estados como exito, error o warning una opcion es dejar que te indiquen el estado en una propiedad o en atributo, pero recuerda dejar @apply comodos de cambiar el aspecto por defecto 
  - Si tu componente utiliza <slot> pon ejemplos de como seria el contenido que tiene un una app para dar mas contexto al uso de tu componente, recuerda tambien que es una mala practica poner en tu componente estilos que afecten al content, lo que se debe de hacer es poner clases y @apply y usar el theme para cambir lo que sea necesario, de lo contrario rompes la encapsulacion y conviertes en una tarea muy dificil reusar tu componente ya que cuando el content es personalizado y muy diferente del que era en tu caso practicamente hay que hacer un reset css de las reglas que aplicas. 













