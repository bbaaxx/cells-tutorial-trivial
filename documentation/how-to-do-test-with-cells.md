## Test unitarios con cells

Cuando desarrollamos un componente de Cells es muy importante comprobar nuestro código con las pruebas que garanticen un funcionamiento adecuado. En esta guía vamos a explicar cómo añadir pruebas de unidad en nuestro código.

Recuerda que el objetivo de las pruebas es determinar que el componente hace lo que tiene que hacer, y para ello vemos que sus diferentes partes se comportan como esperamos, nuestras propiedades, nuestras funciones...

Es muy importante que los test sirvan para que cuando otro desarrollador modifique el codigo, los test fallen si ahora no se cumplen los test que has hecho, de forma que te enteres automaticamente.


#### ¿Que son los test unitarios?

Los test unitarios estan pensados para que en vez de probar nuestro codigo de forma manual, hagamos codigo que pruebe por nosotros.

Cuando desarrollas un fichero de javascript, un modulo o un objeto, al final tienes una serie de propiedades y una serie de metodos, los metodos pueden ser llamados desde fuera de nuestro js o por otros metodos.

Para las propiedades tenemos que comprobar que:
 - estan definidas
 - tienen el valor inicial que esperamos 
 - son del tipo que esperamos

Para los metodos tenemos que comprobar que:
 - despues de ser llamados las propiedades que cambian tienen los nuevos valores que esperamos
 - se han llamado a otros metodos 
 - se han disparado los eventos que esperamos

Puedes obtenr mas informacion sobre como hacer test unitarios en:

- [guia para hacer test en javascript](http://www.integralist.co.uk/posts/guide-to-js-testing.html)
- [Como no romper cosas gracias a los test](https://medium.freecodecamp.com/how-test-driven-development-increased-my-confidence-of-shipping-new-code-without-breaking-things-a759a570bd95)


#### ¿Como escribimos nuestras funciones para que sean faciles de testear?

Imagina que trabajas solo, tienes que hacer un proyecto para ti mismo, haces tu codigo, lo subes a tu servidor y los problemas que ocurran son tuyos, es tu responsabilidad y no necesitas ni colaboras con otros.

En este caso, tu codigo puede ser como quieras y tu documentacion estar escrita a mano en una libreta en diferentes paginas, pero en caso de que trabajes con otros eso no es suficiente, tienes que escribir el codigo de forma que otros lo entiendan y de forma que puedas trabajar con ellos mas facilmente.

En el caso de los test unitarios ocurre lo mismo, el codigo que normalmente escribes puede hacer que tardes mucho en hacer los test y que te obligue a trabajar mas tiempo haciendo test que haciendo codigo.

Afortunadamente hay trucos para escribir el codigo de forma que sea mas facil testear, casualmente casi todas las recomendaciones coinciden con buenas practicas de programacion y lo que debes de hacer si trabajas con mas compañeros.

- no escribas funciones grandes, si tu codigo es pequeño es ams facil saber lo que hace y el numero de test que tienes que escribir para cada funcion disminuye
- haz que tus funciones hagan solamente una cosa, de forma que tus test no tengan que probar todas las posibles combinaciones de cosas que hace tu funcion 
- haz que tus funciones sean lo mas puras posibles, si tus funciones no modifica mas que los parametros que reciben y cambian el valor de una propiedad, solo tienes que probar que pasando los diferentes aprametros esperados se retorna o se modifica la propiedad adecuadamente, si necessitas de variables externas o estados concretos o de otros componentes probar tu componente se vuelve muy dificil
- aisla las llamadas a otros componentes en funciones especificas, si en tus funciones en vez de utilizar otros objetos, cambiando sus propiedades o llamando a sus metodos, solo haces una llamada a otra funcion cuya unica mision es eso, tu funcion no depende de "ese" objeto externo, solo tienes que testear que llamaa a tu otra funcion y las pruebas asociadas a lo slementos externos estan separadas en otros test aparte de tu logica.
- no tengas funciones con muchos if cases o condiciones anidadas, de esta forma tus test seran mas sencillos, si tienes que poner un case  o varias condiciones haz una funcion separada que a partir de los parametros que usas para decidir llame a otras funciones en vez de mezclar decisiones de que hacer, con lo que tienes que hacer.
- define claramente la responsabilidades de tu componente, aveces es necesario dividir el componete en varios o tener helpers.


Puedes ver mas ejemplos de como escribir codigo testeable:

- [como escribir codigo testeable](https://www.toptal.com/qa/how-to-write-testable-code-and-why-it-matters)
- [Writing Testable Code](http://www.methodsandtools.com/archive/archive.php?id=103)

### Como funcionan los test en cells

Los test en cells se desarrollan utilizando el framework Mocha y la biblioteca Chai .

Mocha es un framework de pruebas que permite estructurar las pruebas en bloques. Utilizamos una interfaz TDD para desarrollar nuestras pruebas . 

La documentacion de Polymer sobre test (en ingles) es un punto de referencia interesante para aprender y consultar

 - documentación de como hacer test de [Polymer 2.0](https://www.polymer-project.org/2.0/docs/tools/tests)
 - documentación de como hacer test de  [Polymer 1.0](https://www.polymer-project.org/1.0/docs/tools/tests)

 Otros articulos interesantes son:
 
 - conceptos basicos de [test con polymer](https://www.utest.com/articles/test-your-elements-with-web-component-tester-polymer)
 - ejemplo de test de [componentes](https://medium.com/google-developer-experts/polymer-unit-testing-d6a69910dc31)
 - como hacer testing algunos [tips](https://medium.com/google-developer-experts/polymer-testing-tips-f217ba94a64)
 - videos sobre como hacer test con polymer ([polycast])(https://www.youtube.com/watch?v=YBNBr9ECXLo)


Puedes ver un ejemplo de test en componentes cells en: 

 - test del componente [cells-icon-message](https://globaldevtools.bbva.com/bitbucket/projects/BGCM/repos/cells-icon-message/browse/test/basic-test.html)
 - test del componente [cells-selec](https://globaldevtools.bbva.com/bitbucket/projects/BGCM/repos/cells-select/browse/test/cells-select.html)
 - test del componente [cells-product-item](https://globaldevtools.bbva.com/bitbucket/projects/BGCM/repos/cells-product-item/browse/test/cells-product-item-test.html)

#### Tipos de test en cells

En cells distinguimos tres tipos de test que debemos de hacer, y cada uno de ellos va en un fichero separado 

 - basic-test.html: en este fichero se comprueban las propiedades que tiene tu componente, que valores iniciales tienen y de que tipos
 - function-test.html: en este fichero pruebas las funciones que tienes en tu componente, cada una de ellas es llamada varias veces pasandole los diferentes valores que pueden recibir para verificar que hacen lo que necesitas (en este fichero no se interactua con el dom)
 - dom-test.html: en este test se comprueba que el dom del componete que hemos hecho se comporta como esperamos, verificamos que los eventos del usuario llaman a las funciones que esperamos y que los cambios de las propiedades de nuestro componente se reflejan adecuadamente.

 Es muy importante tener el concepto claro de "no pruebes que polymer funciona", esto es un mito que circula por bastantes desarrolladores de polymer. 

Lo que deseas probar es que tu componete se comporta como esperas, deseas que si tu u otro cambia tu componente y ya no se comporta adecuadamente un test falle.

No es necesario que compruebes que un binding funciona, lo que pruebas es que al cambiar la propiedad a, en el html se pone la clase b por ejemplo.

Algunos casos de test que si debes hacer son:

 - probar que si la condicion se cumple se ocultan los elementos
 - probar que si cambias un array o un objeto se actualiza el dom con los nuevos valores
 - que al dispararse una computada o un observe el dom se actuliza 
 - que las traduciones funcionan
 - que los estados de "aun no me han pasado valores" se muestran como esperamos

#### En que partes esta dividido un test 

Esta interfaz nos proporciona una API de los métodos que se utilizan para organizar nuestro código:

 - suite( ) : contiene un conjunto (suite) de pruebas .
 - test() : contiene una prueba
 - suiteSetup ( ) : contiene código que se ejecutará antes de la próxima suite.
 - suiteTeardown ( ) : contiene código que se ejecutará después de la próxima suite.
 - setup ( ) : contiene código que se ejecuta antes de que todas las suites .
 - teardown ( ) : contiene código que se ejecuta después de todas las suites .


###### ejemplo de test suite
![](images/ejemplo_test_suite.png)

En el ejemplo de la imagen:

 - el código de suiteSetup () y suiteTeardown ( ) se ejecutará antes y después 'Suite 1 '. 
 - setup () y teardown ( ) se ejecutarán antes y después de todas las suites .
 - setup () y teardown ( ) se utilizan generalmente para lanzar código que restablecer nuestros componentes antes y después de cada prueba .
 - suiteSetup () y suiteTeardown ( ) se utilizan generalmente para preparar nuestro medio ambiente para nuestras pruebas ( obtener referencias DOM, se mokean los datos de carga , etc.) .

Tenemos una suit padre que podría ser depósitos y dos suites hijas que podrían ser los test de contratar un deposito y ver la información de un depósito 

#### Chai

Chai es una biblioteca afirmación de que permite verificar ciertas situaciones que deben suceder cuando se ejecuta nuestro código. 

[chaijs](http://chaijs.com/api/bdd/)

Esta biblioteca nos permite escribir los casos de uso que queremos probar de una forma sencilla y bastante parecida a como lo explicarias hablando con otra persona

###### ejemplo de test suite
![](images/ejemplo_aserciones_chai.png)


Generalmente con los casos de uso se comprueba:

 - que los objetos existen
 - que las variables están inicializadas con los valores adecuados 
 - que cuando se llama a una función está retorna el valor correcto 
 - se han modificado las propiedades adecuadamente despues de una llamada a una funcion
 - se han disparado los eventos correctos con sus propiedades ante las acciones del usuario o el cambio de alguna propiedad


#### Test asíncronos
Cuando tenemos un componente o una función, no siempre el resultado se obtiene inmediatamente, a veces hay que hacer una llamada http o esperar a que un evento se dispare, estos comportamientos se denominan asincronos.

En los test para indicar este tipo de comportamientos en los que necesitamos comprobar con un expect un resultado pero no inmediatamente tenemos la función done, llamamos esta funcion cuando sabemos que la peticion asincrona ha acabado, en un ejemplo:

-
~~~ 
// con una funcion cuyo resultado no obtenemos de forma sincrona despues de llamarla 
function asyncFunction(callback) {
  setTimeout(function(){
      callback("ready")
  }, 1000);
}

test('async test', function(done) {
  // llamamos a la funcion pasandole una funcion que se ejecutara como callback cuando termine 
	asyncFunction(function(param){
	// comprobamos el resultado 
	  expect(param).to.equal("ready");
	// llamamos a done para que chai sepa que hemos terminado 
	  done();	
	});
});
~~~

La función setTimeout de javascript espera  un tiempo determinado para después de ese tiempo ejecutar el código de dentro de la función.

No olvides de en el test pasar el parámetro done para que la función exista dentro del test

#### Probando templates dom-*

Al escribir las pruebas de Polymer , podemos encontrarnos en una situación en la que un bloque de dom-repeat o dom-if tienen que ser probados.

Estos bloques de código se cargan de forma asíncrona y se necesita un mecanismo especial que esperar mientras se cargan .

Polymer proporciona el método flush () que ejecuta el código en sí mismo cuando todos los datos se carga en el DOM .

-
~~~ 
    <div> Employee list: </div>
    <template is="dom-repeat" items="{{employees}} id="employeList">
        <div>First name: <span>{{item.first}}</span></div>
        <div>Last name: <span>{{item.last}}</span></div>
    </template>

test("comprobar que el numero de elementos cambia", function(done) {
  // seleccionamos el repeater 
    var domRepeat = document.queryselectorAll('#employeList');
    // seteamos el nuevo valor 
    myEl.set("employees",[
                          {first:'Guzman',last:'Paniagua'},
                          {first:'Noe',last:'Villegas'}
                        ]);
  // le decimos al test que espere a que el dom se refresque 
    flush(function () {
       // comprobamos el nuevo numeor de elementos 
        assert.equal(domRepeat.length, 2);
        // indicamos al test que terminamos
        done();
    }
});

~~~

#### Espias

Cuando comprobamos con tets nuestro componente, también debemos probar si interacciona con el resto de elementos que hay adecuadamente, pero sin crear esos elementos, por ejemplo si mi componente tiene un botón y al pulsarlo se abre una modal mi test no abre la modal, solo comprueba que se llama a la función abreModal().


-
~~~ 
// si tenemos un objeto con sus metodos y sus propiedades 
var user = {
  ...
  setName: function(name){
    this.name = name;
  }
}

//Creamos un espia para que cuando alguien llame al metodo setName del objeto user, no se haga la llamada real sino que se llame al metodo de sinon y luego podamos testear que ha pasado 
var setNameSpy = sinon.spy(user, 'setName');

//Ahora simulamos en nuestro test que se ejecuta la llamada
user.setName('Darth Vader');

//podemos comprobar con los metodos de sinon que se ha llamado al metodo 
console.log(setNameSpy.callCount); //output: 1

//Es importante que despues de testear eliminemos el espia para que el resto de test no se vean afectados 
setNameSpy.restore();
~~~


algunos de los metodos mas usados con espias son 

 - notCalled
 - called
 - calledOnce
 - callCount
 - calledWith

Un buen tutorial sobre sinon con una buena seccion sobre espias es:
[sitepoint](https://www.sitepoint.com/sinon-tutorial-javascript-testing-mocks-spies-stubs/)



## Recetas

###### Como comprobar si una propiedad existe 

Definimos un caso en el que aseguramos que hemos puesto correctamente la propiedad, de esta forma si alguien modifica el codigo y elimina el valor por defecto que tenemos acordado en el contrato, el test fallara y podremos decidir si ya no hace falta la propiedad y eliminamos el test o es un error y hay que volver a poner la porpiedad

-
~~~ 
// con un componente que tiene una propiedad "swipeThreshold"
(function() {
  /* global CellsBehaviors */
  'use strict';

  Polymer({
    is: 'cells-card-detail',
    properties: {
      swipeThreshold: {
        type: Number,
        value: 40
      }
    }
  });
}());

test('should have "is" property equal to "cells-card-detail"', function() {
    expect(myEl).to.have.property('swipeThreshold');
});
~~~

###### Como comprobar si una propiedad tiene el valor inicial correcto 

Es necesario que comprobemos que a una propiedad que utilizamos en nuestro componente le hemos dado un valor por defecto que hemos acordado, si por error modificamos el componente y eliminamos el valor por defecto y quienes utilizan el componente no le dan valor a la propiedad que es obligatoria, el componente no se comportara correctamente 

-
~~~ 
// con un componente que tiene una propiedad "mediaQuery"
(function() {
  /* global CellsBehaviors */
  'use strict';

  Polymer({
    is: 'cells-card-detail',
    properties: {
       mediaQuery: {
        type: String,
        value: '(max-width: 48rem)'
      }
    }
  });
}());

test('should have "is" property equal to "cells-card-detail"', function() {
    expect(myEl.mediaQuery).to.be.equal('(max-width: 48rem)');
});


// otros ejemplos 
	test('should have swipeThreshold property', function() {
	  expect(myEl.swipeThreshold).to.be.equal(40);
	});
	test('should have "percentageBarType" property', function() {
	  expect(myEl.percentageBarType).to.be.equal.true;
	});
~~~



###### Como comprobar si una propiedad esta definida con el tipo correcto 

Es necesario que comprobemos que hemos forzado a que el tipo de las variables es el que necesitamos, en el ejemplo recibimos un array de elementos que se utiliza en un repeater para mostrar la lista, si en lugar de un array al utilizar el componente nos pasan un objeto, el componente fallara, por eso es necesario que con este tipo de test garantizamos que el usuario final del componente recibira un  error si no pasa los tipos correctos a las propiedaddes

-
~~~ 
// con un componente que tiene una propiedad "card" que se utiliza en un repeater
(function() {
  /* global CellsBehaviors */
  'use strict';

  Polymer({
    is: 'cells-card-detail',
    properties: {
      cards: {
        type: Array,
        value: function() {
          return [];
        }
      }
    }
  });
}());

test('should have "is" property equal to "cells-card-detail"', function() {
    expect(myEl.cards).to.be.a('array');
});

// otros ejemplos 
test('should have selected property', function() {
  expect(myEl.selected).to.be.a('object');
});
test('should have openedBalance property', function() {
  expect(myEl.openedBalance).to.be.a('boolean');
});
test('should have storeUrl property', function() {
  expect(myEl.storeUrl).to.be.a('string');
});

~~~

###### Como comprobar que una funcion de tu componente dispara un evento 

Una de las partes mas importantes a probar es que tu componente se comunica corrrectamente con otros a traves de los eventos disparados.

Es nncesario comprobar que se llama al metodo fire, y que este recibe los parametros que esperas.

-
~~~ 
// con un componente que tiene una propiedad "card" que se utiliza en un repeater
(function() {
  /* global CellsBehaviors */
  'use strict';

  Polymer({
    is: 'cells-card-detail',
    /**
    * Fire event with the item name
    */
    _fireSelection: function(item) {
      this.fire(this.notifyEvent, item);
    }
  });
}());

// ejemplo de espia comentado 
	test('Executed _fireSelection function', function() {
    // utilizamos sinon para escuchar cuando se llama al metodo fire de la template
	  sinon.spy(myEl,'fire'); 
	// llamamos a la funcion a testear
	  myEl._fireSelection(); 
	// comprobamos con sinon que ha sido llamado 
	  expect(myEl.fire).called; 
	// comprobamos que el fire ha sido llamado con los parametros que esperamos 
	  expect(myEl.fire.args[0][0]).to.equal('on-click-tab'); 
	// limpiamos el espia que hemos puesto con sinon para que el resto de test no se vean afectados por este 
	  myEl.fire.restore();
	});
~~~

###### Como comprobar que una funcion hace mas de un fire 

En caso de que hagas una funcion de dispacher, es probable que necesites que se disparen diferentes eventos, por ejemplo que se haga la navegacion y que se haga el tagueo.

Para esto sinon nos ofrece la funcion getCall que nos permite recuperar cada una de las diferentes veces que ha sido disparado el evento espiado

-
~~~ 

// En este caso fire es llamado varias veces y usamos getCall para verrificar los diferentes eventos
  test('selected tabis posted', function() {
    sinon.spy(myEl, 'fire');
    myEl.selectTab = 'posted';

    myEl._notifyAccountDetail({});

    expect(myEl.fire).called;
    expect(myEl.fire.getCall(0).args[0]).to.equal('init-pull-refresh');
    expect(myEl.fire.getCall(1).args[0]).to.equal('set-header-title');
    expect(myEl.fire.getCall(2).args[0]).to.equal('get-menu-iwant');
    expect(myEl.fire.getCall(3).args[0]).to.equal('get-account-detail-tabs');

    myEl.fire.restore();
  });

~~~

###### Como comprobar varios parametros y que funcionan los click en vez de llamar a  la funcion directamente 

En caso estamos intentando ver que al pulsar el enlace, se dispara el evento de editar alias.

A mi me gusta mas comprobar que el click llama a la funcion que toca y en otro test olvidarme del html y llamar yo a la funcion pasandole los parametros que me interesa de forma que testeemos de forma separada que el html es correcto y que nuestra funcion es correcto.

Tambien vemos como accediendo a args[0],args[1].. podemos ver los diferentes parametros 

-
~~~ 
// otro test 
  test('should fire event to edit alias with title.name as event param when indicotarId is \'EDITABLE_ALIAS\'', function(done) {
    flush(function() {
     sinon.spy(myEl, 'fire');
      product.alias = null;
      myEl.querySelectorAll('div')[0].querySelector('a').click();
      expect((myEl.fire).to.have.been.called.once;
      expect((myEl.fire.getCall(0).args[0]).equals('editable-alias');
      expect((myEl.fire.getCall(0).args[1]).deep.equals('Young account.');
      done();
    });
  });
~~~


###### Como comprobar cuando los parametros de un fire es un objeto complejo 

En caso que vemos aqui el evento recibe dos parametros el nombre del evento y un objeto de comfiguracion, podriamos poner una linea por cada propiedad e ir viendo que todas ellas tienen los valores que esperamos, pero es mas facil si usamos la ".is.to.deep.equal" de chai para comprobar que el objeto es todo igual

-
~~~ 
        test('Should fire event when user clock accept control', function() {
          myEl._onAccept();
          expect(myEl.fire).called;
          expect(myEl.fire.args[0][0]).is.to.equal('alert-1-accept');
          expect(myEl.fire.args[0][1]).is.to.deep.equal({detail: true});
        });

~~~

###### Como comprobar que un evento se ha llamado solo una vez 

En caso de que un evento solo deba de ser llamado una vez, puedes comprobaarlo con la propiedad "calledOnce" de chai, esto es bastante habitual cuando tienes que disparar un evento solo cuando el componente se inicializa y no el resto de veces.

-
~~~ 
        test('_timeoutHandler function should be called when timeout is more than 0', function() {
          sinon.spy(myEl, '_timeoutHandler');
          myEl.set('opened', false);
          myEl.set('timeout', 5);
          myEl.set('opened', true);
          myEl.set('opened', true);
          expect(myEl._timeoutHandler).calledOnce;
        });

~~~


###### como esperar a que polymer refresque el html 


Determinadas operaciones como pintar los elementos de un repeaater cuando el array de que esta asociado a el cambia, no se realizan de forma inmediata, de forma que si cambias el array para que tenga 10 elementos y miras en el dom para comprobar que hay 10 elementos, te va a decir que el test falla

Cuando vayas a la consola o inspeciones el html, veras que tu codigo si funciona, dando el test un "falso positivo", para evitar esto y que chai sepa que tiene que esperar a que el dom sea actualizado utilizas la funcion flush.

Fijate que hemos pasado  a la funcion del test el parametro done y que es utilizado dentro de flush para indicar a chai que tu test termino

-
~~~ 
    test('check physical item size', function(done) {
      var setSize = 10;
      container.data = buildDataSet(setSize);

      flush(function() {
        assert.equal(list.items.length, setSize);
        done();
      });
    });

~~~

