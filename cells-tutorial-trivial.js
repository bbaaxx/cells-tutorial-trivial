(function() {

  'use strict';

  Polymer({

    is: 'cells-tutorial-trivial',
    properties: {
     /**
     * question object
     has the information of a question
     questionId: the id for know wich question is
     questionIndex: is the number of question in total
     questionTotal: the total of questions of a test
     title1: the question text
     answers: the array of answers
     answers[n].text: the text to show to the user
     answers[n].id: the id for the answer, used to know which answer is correct

     json  example
     {
          questionId:'3423212',
          questionIndex:0,
          questionTotal:10,
          title1:'Enunciado de la pregunta',
          answers:[
            {'text':' respuesta 1', id:'12124213'},{'text':' respuesta 2',id:'1432321'}
          ]
        }
     */
      question: {
        type: Object,
        value: function() {
          return {
            questionId: '3423212',
            questionIndex: 0,
            questionTotal: 10,
            title1: 'Enunciado de la pregunta',
            answers: [
              {'text': 'respuesta 1', id: '12124213'},
              {'text': ' respuesta 2', id: '1432321'},
              {'text': ' respuesta 3', id: '3223423'}
            ]
          };
        }
      },
      /**
      index for the selected answer, comes from behabior of paper-listbox
      */
      selectedIndex: {
        type: Object
      }
    },
    answer: function() {
      if (this.selectedIndex !== undefined) {
        this.fire('answer-selected', {'questionId': this.questionId, 'selectedIndex': this.selectedIndex});
      }
    }
  });

}());
